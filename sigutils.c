
#include "basics.h"
#include "numbers.h"
#include "squares.h"
#include <unistd.h>

#define VERSION 0.01

void version_msg ()
{
  puts("Sigil Utilities, version 0.01\n"
       "(C) 2020 theblaqcksquid <theblacksquid@subvertising.org>\n"
       "Licensed under GNU GPL v3\n");
}

void help_msg ()
{
  puts("usage: sigutils [options] <STATEMENT OF INTENT>\n"
	"-h     Print this message and quit.\n"
	"-v     Print the version and license info then quit.\n"
	"-n     Append the numerical and root values to the result.\n"
	"-w     Generate a Word Square based on the SOI\n"
	"-W     Generate word square ONLY.\n");
}

int main(int argc, char** argv)
{
  int numeric_flag = 0;
  int version_flag = 0;
  int help_flag = 0;
  int word_flag = 0;
  int word_only_flag = 0;
  int c;

  while ((c = getopt(argc, argv, "nvhwW")) != -1)
    switch (c)
      {
        case 'n': numeric_flag = 1;
	  break;
      
        case 'v': version_flag = 1;
	  break;

	case 'h': help_flag = 1;
	  break;

	case 'w': word_flag = 1;
	  break;
	   
	case 'W': word_only_flag = 1;
	  break;
      }

  if (version_flag)
    {
      version_msg();
      return 0;
    }

  else if (help_flag)
    {
      help_msg();
      return 0;
    }

  char* raw = absorb_words(argc, argv, optind);
  char* result = string_process(raw);

  if (!word_only_flag)
    printf("%s", result);

  if (numeric_flag)
    {
      int value = string_value(result, LETTERS);
      int root = digital_root(value, 10);
      printf(" %d %d", value, root);
    }

  if (word_flag || word_only_flag)
    {
      if (!word_only_flag)
	printf("\n\n");
      printf("%s", generate_square(result));
    }

  printf("\n");

  free(result);
  free(raw);
  return 0;
}

