
#ifndef BASICS_H
#define BASICS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char* absorb_words (int count, char** string_list, int start_index)
{
  char* result = calloc(sizeof(char), (count * 20));
  for (int i = start_index; i < count; i++)
    strcat(result, string_list[i]);

  return result;
}

char* __upcase (char* original)
{
  char* result = calloc(sizeof(char), strlen(original));
  int index = 0;
  while (original[index])
    {
      result[index] = toupper(original[index]);
      index++;
    }

  return result;
}

char* strip_nonalpha (char* original)
{
  int length = strlen(original);
  char* result = calloc(sizeof(char), length);

  int count = 0;
  for (int i = 0; i < length; i++)
    {
      if (isalpha(original[i]))
	{
	  result[count] = original[i];
	  count++;
	}
    }

  return result;
}

char* strip_dupes (char* original)
{
  int length = strlen(original);
  char* upcased = __upcase(original);
  char* result = calloc(sizeof(char), 26); // 26 unique characters in
					   // the english alphabet
  int count = 0;
  for (int i = 0; i < length; i++)
    {
      if (!strchr(result, (int) upcased[i]))
	{
	  result[count] = upcased[i];
	  count++;
	}
    }

  free(upcased);
  return result;
}

char* string_process (char* string)
{
  char* filtered = strip_nonalpha(string);
  char* result = strip_dupes(filtered);

  free(filtered);
  return result;
}

#endif
