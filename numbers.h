
#ifndef NUMBERS_H
#define NUMBERS_H

typedef struct
{
  char id;
  int val;
} assoc;

char LETTERS[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

assoc letter_value (char id, char* ref)
{
  char* letters = ref;
  
  for (int i = 0; i < 26; i++)
    {
      if (letters[i] == id)
	return (assoc) {id, i + 1};
    }
}

int string_value (char* string, char* ref)
{
  int result = 0;
  int length = strlen(string);

  for (int i = 0; i < length; i++)
    {
      assoc val = letter_value(string[i], ref);
      result += val.val;
    }

  return result;
}

int digital_sum (int num, int base)
{
  int result = 0;
  while (num > 0)
    {
      result = result + (num % base);
      num = num / base;
    }

  return result;
}

int digital_root (int num, int base)
{
  while (num > base)
    num = digital_sum(num, base);

  return num;
}

#endif
