
#include "basics.h"
#include <math.h>

int is_even (int num)
{
  return (num % 2);
}

char* inside_out (char* string)
{
  int length = strlen(string); 
  int center = is_even(length);
  char* result = calloc(sizeof(char), length + 1); // for NUL
  int half = (length / 2) - 1; // account for 0-index
  
  for (int i = 0; i < length; i++)
    {
      if (i == half)
	result[i] = string[0];

      else if (center && (i == (half + 1)))
	result[i] = string[half + 1];

      else if (i == (half + 1 + center))
	result[i] = string[length - 1];

      else if (i <= half)
	result[i] = string[i + 1];

      else
	result[i] = string[i - 1];
    }

  return result;
}


char* generate_square(char* string)
{
  int length = strlen(string);
  // The size is for the square, the newlines and the NUL-term
  int result_size = (length * length) + length;
  char* result = calloc(sizeof(char), result_size);

  char* current = string;
  int count = 0;
  int index = 0;
  for (index = 0; index < (result_size / 2); index++)
    {
      if (count == length)
	{
	  result[index] = '\n';
	  current = inside_out(current);
	  count = 0;
	}

      else
	{
	  result[index] = current[count];
	  count++;
	}
    }

  count = (result_size / 2) - 1;
  for (index = (result_size / 2) - 1; index < result_size; index++)
    {
      // printf("\nprocessing half...\n");
      result[index] = result[count];
      count--;
    }

  return result;
}
