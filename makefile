
CFLAGS= -Wall -Wextra -g
FILES= sigutils.c

all:
	gcc ${FILES} ${CFLAGS} -o sigutils

install:
	rm ~/bin/sigutils && \
	cp sigutils ~/bin
